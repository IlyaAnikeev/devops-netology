# devops-netology
# Modified
Исключить указанные файлы находящиеся в дирректории **/.terraform/*

Исключить все файлы с расширением *.tfstate (11111.tfsstate и т.д.)
Исключить все файлы *.tfstate.* (11111.tfstate.111111 и т.д.)
Исключить файлы журнала сбоев crash.log и crash.*.log (crash.11111.log и т.д)
Исключить файлы с возможным расположением конфидециальных данных с расширением *.tfvars и *.tfvars.json (11111..tfvars и 11111.tfvars.json и т.д.)
Игнорировать файлы override.tf, override.tf.json, *_override.tf, *_override.tf.json (11111_override.tf, 11111_override.tf.json)
Игнорировать конфигурационные файлы CLI с расширением .terraformrc и terraform.rc (11111.terraformrc)

add new line in PyCharm
